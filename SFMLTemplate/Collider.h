#pragma once
#include <SFML\Graphics.hpp>

class Collider
{
public:
	Collider();
	~Collider();

	bool CheckCollision(sf::Rect<float>& boundingBox, sf::RectangleShape* shapeToCheck);

	void SetIsStanding() { m_IsStanding = true; }
	bool GetIsStanding() { return m_IsStanding; }

	bool GetContactTop() { return m_ContactTop; }
	bool GetContactBottom() { return m_ContactBottom; }
	bool GetContactLeft() { return m_ContactLeft; }
	bool GetContactRight() { return m_ContactRight; }
	void SetAllContactsFalse();

private:
	bool m_ContactTop = false,
		m_ContactBottom = false,
		m_ContactRight = false,
		m_ContactLeft = false,
		m_IsStanding = false;
};
