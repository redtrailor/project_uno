#pragma once
#include "stdafx.h"
#include "EquippedInventory.h"
#include "Player.h"


EquippedInventory::EquippedInventory()
{
	Init();
	m_RobeSlot.reset(new Brown_Robe);
	m_ChestSlot.reset(new Leather_Chest);
	m_WeaponSlot.reset(new Steel_Sword);
	m_HeadSlot.reset(new Leather_Helm);
	
}


EquippedInventory::~EquippedInventory()
{
	
}

void EquippedInventory::Init()
{
	FlipSprite = false;
}

void EquippedInventory::Render(sf::RenderWindow& window)
{
	if (m_HeadSlot != nullptr)
		window.draw(m_HeadSlot->GetPlaySprite());
	if (m_RobeSlot != nullptr)
		window.draw(m_RobeSlot->GetPlaySprite());
	if (m_ChestSlot != nullptr)
		window.draw(m_ChestSlot->GetPlaySprite());
	if (m_OffhandSlot != nullptr)
		window.draw(m_OffhandSlot->GetPlaySprite());
	if (m_WeaponSlot != nullptr)
		window.draw(m_WeaponSlot->GetPlaySprite());
}

void EquippedInventory::Update(Player& player/*const sf::Vector2f playerPos, int column, int row, bool isAttacking*/)
{
	if (m_HeadSlot != nullptr)
		m_HeadSlot->Update(player.GetCurrentPosition(), player.GetColumn(), player.GetRow(), FlipSprite, player.GetAttacking());
	if (m_RobeSlot != nullptr)
		m_RobeSlot->Update(player.GetCurrentPosition(), player.GetColumn(), player.GetRow(), FlipSprite, player.GetAttacking());
	if (m_ChestSlot != nullptr)
		m_ChestSlot->Update(player.GetCurrentPosition(), player.GetColumn(), player.GetRow(), FlipSprite, player.GetAttacking());
	if (m_WeaponSlot != nullptr)
		m_WeaponSlot->Update(player.GetCurrentPosition(), player.GetColumn(), player.GetRow(), FlipSprite, player.GetAttacking());
	
	SetFlip(false);
}
