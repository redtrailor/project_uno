#include "stdafx.h"
#include <SFML\Graphics.hpp>
#include "RunningState.h"
#include "StandingState.h"
#include "CrouchState.h"
#include "Player.h"


RunningState::RunningState()
{
}

RunningState::~RunningState()
{
}

PlayerState* RunningState::InputHandler(Player& player, PlayerState* state, sf::Time& deltaTime)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		return new CrouchState;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		return new StandingState;
	}

	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::D) && !sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		return new StandingState;
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !player.GetAttacking())
	{
		player.SetAttacking(true);
		player.SetColumn(2);
		return new StandingState;
	}
	Jump(player);
	HorizontalDirectionHandler(player, deltaTime);

	if (!player.m_Collider.GetIsStanding())
	{
		return new AirborneState;
	}
	//return PlayerState::InputHandler(player, state, deltaTime);

	return nullptr;
}

void RunningState::Update(Player & player, sf::Time & deltaTime)
{
	//	Running animation Loop
	tempDeltaTime += deltaTime.asSeconds();
	if (tempDeltaTime > .1f)
	{
		player.SetColumn(player.GetColumn() + 1);
		tempDeltaTime = 0;
		if (player.GetColumn() > 7)
		{
			player.SetColumn(0);
		}
	}

	HorizontalMoveHandler(player, deltaTime);
}

void RunningState::Enter(Player& player, sf::Time& deltaTime)
{
	isActive = true;
	player.SetAttacking(false);

	player.SetRow(3);
	player.SetVerticalMovementSpeed(0);
	m_StateHorizontalSpeed = 200;
}

void RunningState::Exit(Player & player, sf::Time & deltaTime)
{
}

void RunningState::ChangeState()
{
}