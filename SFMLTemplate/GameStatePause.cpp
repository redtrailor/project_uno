#include "stdafx.h"
#include "GameStatePause.h"


GameStatePause::GameStatePause(GameManager* game)
{
	m_Game = game;
}


GameStatePause::~GameStatePause()
{
}

void GameStatePause::HandleInput(sf::RenderWindow& window)
{
	sf::Event event;

	while (window.pollEvent(event))
	{
		switch (event.type)
		{

		case sf::Event::Closed:
		{
			window.close();
			break;
		}

		case sf::Event::KeyPressed:
		{
			if (event.key.code == sf::Keyboard::Escape) m_Game->PopState();
			break;
		}

		}
	}
}

void GameStatePause::Update(sf::Time& deltaTime, sf::RenderWindow& window)
{

}

void GameStatePause::Draw(sf::Time& deltaTime, sf::RenderWindow& window)
{
	window.setView(m_Game->m_View);
	window.clear();
	window.draw(m_Game->m_Background);
	window.draw(m_Game->m_Map);
	window.draw(m_Game->m_Player.playerSprite);

	//render player equipment
	m_Game->m_Player.Render(window);
}

