#include "stdafx.h"
#include "BaseEquipment.h"


BaseEquipment::BaseEquipment()
{

}

BaseEquipment::~BaseEquipment()
{
}

void BaseEquipment::Init(int health, int attack, int defense, int magic, int stamina, Weight weight, Slot slot, std::string playTexture, std::string invTexture)
{
	m_Health = health;
	m_Attack = attack;
	m_Defense = defense;
	m_Stamina = stamina;
	m_Weight = weight;
	m_Slot = slot;
	if (!m_PlayTexture.loadFromFile(playTexture))
	{
		printf("Failed to load armor texture from : %s\n", playTexture);
	}
	if (!m_InventoryTexture.loadFromFile(invTexture))
	{
		printf("Failed to load armor texture from : %s\n", invTexture);
	}

	m_PlaySpriteColumn = 0;
	m_PlaySpriteRow = 0;


}

void BaseEquipment::Render(float column, float row)
{
}

void BaseEquipment::SetStartingDirection(bool isPLayerFacingRight)
{
	
}

