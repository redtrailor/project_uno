#include "stdafx.h"
#include "Weapon.h"


Weapon::Weapon()
{
	Init();
}

Weapon::~Weapon()
{
}

void Weapon::Update(const sf::Vector2f playerPos, int column, int row, bool flipSprite, bool isAttacking)
{
	SetPlaySpritePosition(playerPos);
	if (isAttacking)
	{
		SetPlaySpriteRect(column, 0);
	}
	if (flipSprite == true)
	{
		FlipPlaySprite();

	}
}

void Weapon::Init()
{
	m_PlaySpriteRow = 1;
	m_PlaySpriteColumn = -3;
	m_PlaySprite.setTexture(m_PlayTexture);
	m_PlaySprite.setTextureRect(sf::IntRect(m_PlaySpriteColumn * 60, m_PlaySpriteRow * 30, 60, 30));
	m_PlaySprite.setOrigin(22, 0);
	m_PlaySprite.scale(1.2f, 1.2f);
}


