#ifndef TILEMAP_HEADER
#define TILEMAP_HEADER

#include "stdafx.h"
#include <SFML\Graphics.hpp>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include "Collider.h"


class TileMap : public sf::Drawable, public sf::Transformable
{
public:

	bool LoadFromFile(const std::string& tileSet, sf::Vector2u tileSize, const std::string& fileName, sf::RectangleShape* collider[100])
	{
		int level[1000];

		for (int i = 0; i < 1000; i++)
		{
				level[i] = -1;
		}

		std::ifstream file;

		file.open(fileName);

		if (file.fail())
		{
			return false;
		}

		std::string temp;
		
		int row = 0;
		int column = 0;
		while (std::getline(file, temp))
		{
			std::string current = "";

			for (int i = 0; i < temp.length(); i++)
			{
				if (temp[i] != ',')
				{
					current += temp[i];
				}
				else 
				{
					std::istringstream convert(current);
					int num;
					convert >> num;
					level[row * 20 + column] = num;
					column++;
					current = "";
				}
			}
			row++;
			column = 0;
		}
		

		return load(tileSet, tileSize, level, 20, row, collider);
	}

	bool load(const std::string& tileset, sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height, sf::RectangleShape* collider[100])
	{
		// load the tileset texture
		if (!m_tileset.loadFromFile(tileset))
			return false;

		// resize the vertex array to fit the level size
		m_vertices.setPrimitiveType(sf::Quads);
		m_vertices.resize(width * height * 4);

		int collisionIndex = 0;
		// populate the vertex array, with one quad per tile
		for (unsigned int i = 0; i < width; ++i)
			for (unsigned int j = 0; j < height; ++j)
			{
				// get the current tile number
				int tileNumber = tiles[i + j * width];

				// find its position in the tileset texture
				int tu = tileNumber % (m_tileset.getSize().x / tileSize.x);
				int tv = tileNumber / (m_tileset.getSize().x / tileSize.x);

				// get a pointer to the current tile's quad
				sf::Vertex* quad = &m_vertices[(i + j * width) * 4];

				// define its 4 corners
				quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
				quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
				quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
				quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

				//Take in collider here//
				if (tileNumber == 12)
				{
					collider[collisionIndex] = new sf::RectangleShape(sf::Vector2f(tileSize.x, tileSize.y));
					collider[collisionIndex]->setPosition(sf::Vector2f(i * tileSize.x, j * tileSize.y));
					collisionIndex++;
					//std::cout << "Created bounding box" << std::endl;
				}

				// define its 4 texture coordinates
				quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
				quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
				quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
				quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
			}

		return true;
	}

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		// apply the transform
		states.transform *= getTransform();

		// apply the tileset texture
		states.texture = &m_tileset;

		// draw the vertex array
		target.draw(m_vertices, states);
	}

	sf::VertexArray m_vertices;
	sf::Texture m_tileset;
};
#endif