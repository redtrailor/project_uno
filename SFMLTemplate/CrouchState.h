#pragma once
#include "PlayerState.h"
class CrouchState : public PlayerState
{
public:
	CrouchState();
	virtual ~CrouchState();
	virtual PlayerState* InputHandler(Player& player, PlayerState* state, sf::Time& deltaTime);
	virtual void Update(Player& player, sf::Time& deltaTime);
	virtual void Enter(Player& player, sf::Time& deltaTime);
	virtual void Exit(Player& player, sf::Time& deltaTime);
};