#include "stdafx.h"
#include "PlayerState.h"
#include "Player.h"


PlayerState::PlayerState()
{
	tempDeltaTime = 0;
	
}

PlayerState::~PlayerState()
{
}

PlayerState* PlayerState::InputHandler(Player& player, PlayerState* state, sf::Time& deltaTime)
{
	if (!player.m_Collider.GetIsStanding())
	{
		return new AirborneState;
	}
	return nullptr;
}

void PlayerState::Update(Player & player, sf::Time & deltaTime)
{
}

void PlayerState::Enter(Player & player, sf::Time& deltaTime)
{
}

void PlayerState::Exit(Player & player, sf::Time & deltaTime)
{
}

void PlayerState::ChangeState()
{
}

void PlayerState::HorizontalDirectionHandler(Player & player, sf::Time& deltaTime)
{
	isMovingRight = false;
	isMovingLeft = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// stand still
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		isMovingRight = true;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		isMovingLeft = true;
	}

	if (isMovingLeft && player.GetFacingRight())
	{
		player.playerSprite.scale(-1.f, 1.f);
		player.m_Equipped.SetFlip(true);
	}
	if (isMovingRight && !player.GetFacingRight())
	{
		player.playerSprite.scale(-1.f, 1.f);
		player.m_Equipped.SetFlip(true);
	}

	if (isMovingRight)
	{
		player.SetFacingRight(true);
	}
	else if (isMovingLeft)
	{
		player.SetFacingRight(false);
	}
}

void PlayerState::HorizontalMoveHandler(Player & player, sf::Time & deltaTime)
{
	if (player.m_Collider.GetContactRight())
	{
		isMovingRight = false;

	}
	if (player.m_Collider.GetContactLeft())
	{
		isMovingLeft = false;
	}
	if(player.m_Collider.GetContactTop())
	{
		player.SetVerticalMovementSpeed(0);
		player.playerSprite.move(0, 1);
	}
	if (isMovingRight)
	{
		player.SetHorizontalMovementSpeed(static_cast<float>(m_StateHorizontalSpeed));
	}
	else if (isMovingLeft)
	{
		player.SetHorizontalMovementSpeed(static_cast<float>(-m_StateHorizontalSpeed));
	}
	else
	{
		player.SetHorizontalMovementSpeed(0);
	}
	player.playerSprite.move(sf::Vector2f(player.GetHorizontalMovementSpeed(), player.GetVerticalMovementSpeed()) * deltaTime.asSeconds());
}

void PlayerState::Attack(Player & player, sf::Time & deltaTime, int endColumn, int endRow)
{
	// attack loop shared with run state
	// set a real attack speed variable you twunt
	
	if (player.GetAttacking())
	{
		tempDeltaTime += deltaTime.asSeconds();
		if (tempDeltaTime > .05f)
		{
			player.SetColumn(player.GetColumn() + 1);
			tempDeltaTime = 0;
			if (player.GetColumn() > 5)
			{
				player.SetColumn(endColumn);
				player.SetRow(endRow);
				player.SetAttacking(false);
			}
		}
	}  ////////////////////////////////////////////    end of attack loop
}

PlayerState* PlayerState::Jump(Player& player)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		player.SetVerticalMovementSpeed(-400);
		return new AirborneState;
	}
	return nullptr;
}
