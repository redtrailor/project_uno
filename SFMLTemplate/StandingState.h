#pragma once
#include "PlayerState.h"
#include <SFML\Graphics.hpp>

class Player;

class StandingState : public PlayerState
{
public:
	StandingState();
	virtual ~StandingState();
	virtual PlayerState* InputHandler(Player& player, PlayerState* state, sf::Time& deltaTime);
	virtual void Update(Player& player, sf::Time& deltaTime);
	virtual void Enter(Player& player, sf::Time& deltaTime);
	virtual void Exit(Player& player, sf::Time& deltaTime);

	virtual void ChangeState();

private:
	float tempDeltaTime;
};