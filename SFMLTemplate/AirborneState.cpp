#include "stdafx.h"
#include "AirborneState.h"
#include "Player.h"


AirborneState::AirborneState()
{
}

AirborneState::~AirborneState()
{
}

PlayerState * AirborneState::InputHandler(Player& player, PlayerState * state, sf::Time & deltaTime)
{
	
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::A) && !sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		player.SetHorizontalMovementSpeed(0);
	}
	if (player.m_Collider.GetContactBottom())
	{
		return new StandingState;
	}

	HorizontalDirectionHandler(player, deltaTime);


	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !player.GetAttacking())
	{
		player.SetColumn(2);
		player.SetRow(1);
		player.SetAttacking(true);

	}
	return nullptr;
}

void AirborneState::Update(Player & player, sf::Time & deltaTime)
{
	float tempNewDownForce = player.GetVerticalMovementSpeed() + player.m_Gravity * deltaTime.asSeconds();
	player.SetVerticalMovementSpeed(tempNewDownForce);

	if (!player.GetAttacking())
	{                             //	Airborne Sprite change based on vertical velocity
		if (tempNewDownForce < 0) {
			player.SetColumn(6);
		}
		else
		{
			player.SetColumn(7);
		}
	}
	if (player.m_Collider.GetContactRight())
	{
		isMovingRight = false;

	}
	if (player.m_Collider.GetContactLeft())
	{
		isMovingLeft= false;

	}
	HorizontalMoveHandler(player, deltaTime);

	Attack(player, deltaTime, 6, 0);

}

void AirborneState::Enter(Player & player, sf::Time & deltaTime)
{
	isActive = true;
	player.SetRow(0);
	player.SetColumn(6);
	isMovingRight = false;
	isMovingLeft = false;

	player.SetStandingOnGround(false);
	m_StateHorizontalSpeed = 175;
}

void AirborneState::Exit(Player & player, sf::Time & deltaTime)
{
}

void AirborneState::ChangeState()
{
}