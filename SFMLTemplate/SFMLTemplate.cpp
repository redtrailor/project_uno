// SFMLTemplate.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <SFML\Graphics.hpp>
#include "GameManager.h"
#include "GameStateStart.h"
#include <iostream>
#include <TGUI\TGUI.hpp>

int main()
{
	GameManager myGame;
	myGame.PushState(new GameStateStart(&myGame));
	myGame.Play();
	return 0;
}