#pragma once
#include <string>
#include <vector>
#include <SFML\Graphics.hpp>
class Level
{
public:
	Level(const std::string& fileName);
	~Level();

	void Draw();

	int GetWidth() const { return m_LevelData[0].size(); }
	int GetHeight() const { return m_LevelData.size(); }
	sf::Vector2f GetPlayerPos() const { return m_PlayerStart; }

private:
	std::vector<std::string> m_LevelData;
	sf::Vector2f m_PlayerStart;
	//sf::Vector2<sf::Texture> m_Textures;
	//std::vector<sf::Sprite> m_Sprites;
};

