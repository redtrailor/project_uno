#pragma once
#include <TGUI\TGUI.hpp>
#include <SFML\Graphics.hpp>
#include <iostream>
#include <memory>
#include <stack>
#include "Player.h"
#include "TileMap.h"
#include "Camera.h"
#include "GameState.h"


class GameState;

class GameManager
{
public:
	GameManager();
	~GameManager();

	void Play();

	void PushState(GameState* state);
	void PopState();
	void ChangeState(GameState* state);
	GameState* PeekState();

	sf::Vector2i GetMousePosition(sf::RenderWindow& window);
	void Init();

	sf::RectangleShape* collisionArray[100];

	sf::Sprite m_Background;

	TileMap m_Map;

	tgui::Gui m_GUI; // GUI instantiation

	Player m_Player;
	sf::View m_View;

private:

	std::unique_ptr<sf::RenderWindow> window;
	sf::Clock clock;
	sf::Time m_DeltaTime;
	std::stack<GameState*> m_States;
	sf::Texture m_BackgroundTex;

	sf::Vector2i mousePos;
};