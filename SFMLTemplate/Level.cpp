#include "stdafx.h"
#include "Level.h"
#include <fstream>
#include <iostream>


Level::Level(const std::string& fileName)
{
	std::ifstream file;

	file.open(fileName);

	if (file.fail())
	{
		std::cout << "Failed to open level file...";
		return;
	}

	std::string tmp;

	std::getline(file, tmp);

	while (std::getline(file, tmp))
	{
		m_LevelData.push_back(tmp);
	}
}

Level::~Level()
{

}

void Level::Draw()
{
	for (int y = 0; y < m_LevelData.size(); y++)
	{
		for (int x = 0; x < m_LevelData[y].size(); x++)
		{
			char tile = m_LevelData[y][x];

			switch (tile)
			{
			case '0':
			{
				//m_Textures.push_back();
			}	
			case '1':
			case '2':
			case '3':
			case '4':
			default:
				std::printf("Unexpected symbol %c at (%d, %d)", tile, x, y);
				break;
			}
		}
	}
}
