#pragma once
#include <string>

enum class Weight
{
	VOID,
	LIGHT,
	MEDIUM,
	HEAVY
};

enum class Slot
{
	VOID,
	HELM,
	ROBE,
	CHEST,
	OFFHAND,
	WEAPON,
	BOOTS,
	RING_ONE,
	RING_TWO,
	AMULET
};

class BaseEquipment
{
public:
	BaseEquipment();
	virtual ~BaseEquipment();

	virtual void Init(int health, int attack, int defense, int magic, int stamina, Weight weight, Slot slot, std::string playTexture, std::string invTexture);
	virtual void Render(float column, float row);
	virtual void Update(const sf::Vector2f playerPos, int column, int row, bool flipSprite, bool isAttacking = false) {};

	void SetStartingDirection(bool isPLayerFacingRight);

	//Getters
	sf::Sprite GetPlaySprite() { return m_PlaySprite; }
	int GetHealth() { return m_Health; }
	int GetAttack() { return m_Attack; }
	int GetDefense() { return m_Defense; }
	int GetMagic() { return m_Magic; }
	int GetStamina() { return m_Stamina; }
	Weight GetWeight() { return m_Weight; }
	Slot GetSlot() { return m_Slot; }

	//Setters
	virtual void SetPlaySpritePosition(const sf::Vector2f playerPos) {};
	virtual void SetPlaySpriteRect(int column, int row) {};
	void SetHealth(int val) { m_Health = val; }
	void SetAttack(int val) { m_Attack = val; }
	void SetDefense(int val) { m_Defense = val; }
	void SetMagic(int val) { m_Magic = val; }
	void SetStamina(int val) { m_Stamina = val; }
	void SetWeight(Weight val) { m_Weight = val; }
	void SetSlot(Slot val) { m_Slot = val; }

protected:
	
	std::string m_LoadPlayTexture;
	std::string m_LoadInventoryTexture;
	int m_Health;
	int m_Attack;
	int m_Defense;
	int m_Magic;
	int m_Stamina;

	Slot m_Slot;
	Weight m_Weight;

	sf::Texture m_PlayTexture;
	sf::Texture m_InventoryTexture;
	sf::Sprite m_PlaySprite;
	sf::Sprite m_InventorySprite;
	int m_PlaySpriteColumn;
	int m_PlaySpriteRow;
};