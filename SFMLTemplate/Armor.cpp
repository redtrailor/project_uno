#include "stdafx.h"
#include "Armor.h"
#include <SFML\Graphics.hpp>
#include "Brown_Robe.h"


Armor::Armor()
{
	Init();
}

Armor::~Armor()
{
	
}

void Armor::Update(const sf::Vector2f playerPos, int column, int row, bool flipSprite, bool isAttacking)
{
	SetPlaySpritePosition(playerPos);
	SetPlaySpriteRect(column, row);
	if (flipSprite == true)
	{
		FlipPlaySprite();		
	}
}

void Armor::Init()
{
	m_PlaySprite.setOrigin(23, 0);
	m_PlaySprite.setTexture(m_PlayTexture);
	m_PlaySprite.setTextureRect(sf::IntRect(m_PlaySpriteColumn * 46, m_PlaySpriteRow * 50, 46, 50));
	m_PlaySprite.scale(1.2f, 1.2f);
}
