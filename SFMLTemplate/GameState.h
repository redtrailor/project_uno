#pragma once
#include <TGUI\TGUI.hpp>
#include "GameManager.h"

class GameManager;

class GameState
{
public:
	virtual void HandleInput(sf::RenderWindow& window) = 0;
	virtual void Update(sf::Time& deltaTime, sf::RenderWindow& window) = 0;
	virtual void Draw(sf::Time& deltaTime, sf::RenderWindow& window) = 0;
	
	GameManager* m_Game;
};

