#pragma once
#include "GameState.h"

class GameStatePause : public GameState
{
public:
	GameStatePause(GameManager* game);
	~GameStatePause();

	virtual void HandleInput(sf::RenderWindow& window);
	virtual void Update(sf::Time& deltaTime, sf::RenderWindow& window);
	virtual void Draw(sf::Time& deltaTime, sf::RenderWindow& window);
};

