#include "stdafx.h"
#include "CrouchState.h"
#include "Player.h"


CrouchState::CrouchState()
{
}

CrouchState::~CrouchState()
{
}

PlayerState * CrouchState::InputHandler(Player& player, PlayerState * state, sf::Time & deltaTime)
{
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		return new StandingState;
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !player.GetAttacking())
	{
		player.SetAttacking(true);
		player.SetColumn(2);
		player.SetRow(2);
	}


	return PlayerState::InputHandler(player, state, deltaTime);

	return nullptr;
}

void CrouchState::Update(Player & player, sf::Time & deltaTime)
{
	Attack(player, deltaTime, 1, 0);

}

void CrouchState::Enter(Player & player, sf::Time & deltaTime)
{
	
	isActive = true;
	player.SetStandingOnGround(true);
	if (!player.GetAttacking())
	{
		player.SetRow(0);
		player.SetColumn(1);
	}
}

void CrouchState::Exit(Player & player, sf::Time & deltaTime)
{
}
