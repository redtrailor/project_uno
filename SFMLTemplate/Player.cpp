#pragma once
#include "stdafx.h"
#include "Player.h"
#include <SFML\Graphics.hpp>
#include "PlayerState.h"
#include "StandingState.h"
#include "AirborneState.h"


//Initialize base stats with new Player call
Player::Player() : m_Health(100), m_Attack(10), m_Defense(10), m_Magic(10), m_Stamina(100)
{
	Init();
}


Player::~Player()
{
}


void Player::Init()
{
	state_ = new StandingState;

	spriteRow = 0;
	spriteColumn = 0;
	m_PreviousStates.reserve(5);
	playerSprite.setOrigin(23, 0);
	m_Gravity = 500;
	m_CurrentPosition = playerSprite.getPosition();

	if (!playerTexture.loadFromFile("CharacterSprites/player/player-spritemap-V9-redpants.png"))
	{
		printf("Error Loading Sprite Sheet");
	};

	playerSprite.setTexture(playerTexture);

	playerSprite.setTextureRect(sf::IntRect(spriteColumn, spriteRow, 46, 50));

	//quick and dirty make him bigger
	playerSprite.scale(1.2, 1.2);

	m_isFacingRight = true;
	m_isStandingOnGround = false;
	playerSprite.setPosition(sf::Vector2f(500, 200));

}

void Player::InputHandler(sf::Time& deltaTime)
{

	// teh state you send in here never gets used. would probably break shit.
	PlayerState* state = state_->InputHandler(*this, state_, deltaTime);

	/////////////////////////////////
	//
	//	Gets what the new state will be from input above, checks to see if its null,
	//	deletes the old pointer to free the memory and sets it to the new state.
	//
	/////////////////////////////////

	if (state != nullptr)
	{
		state_->isActive = false;
		if (!state_->isActive)
		{
			state_->Exit(*this, deltaTime);

		}
		delete state_;
		state_ = state;
	}
	state_->InputHandler(*this, state_, deltaTime);

	if (state_->isActive == false)
	{
		state_->Enter(*this, deltaTime);
	}

}

void Player::Update(sf::Event& event, sf::Time& deltaTime)
{
	state_->Update(*this, deltaTime);
	playerSprite.setTextureRect(sf::IntRect(GetColumn() * 46, GetRow() * 50, 46, 50));

	m_Equipped.Update(*this);
	//m_Equipped.Update(this->GetCurrentPosition(),this->GetColumn(),this->GetRow(), this->m_isAttacking);
	//printf("BoundingBox Bottom: %f\n", GetBoundingBox().top + GetBoundingBox().height);
}

void Player::Render(sf::RenderWindow& window)
{

	m_Equipped.Render(window);
}


//yes yes magic numbers because it was tweaked this way
sf::Rect<float> Player::GetBoundingBox()
{
	return sf::Rect<float>(
		playerSprite.getPosition().x - 13,
		playerSprite.getPosition().y + 5,
		25,
		52
		);
}