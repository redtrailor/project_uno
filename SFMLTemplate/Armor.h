#pragma once
#include "BaseEquipment.h"

class Armor : public BaseEquipment
{
public:
	Armor();
	virtual ~Armor();

	virtual void Update(const sf::Vector2f playerPos, int column, int row, bool flipSprite, bool isAttacking);
	void Init();

	int GetDamageReduction() { return m_DamageReduction; }
	void SetDamageReduction(int val) { m_DamageReduction = val; }

	void SetPlaySpritePosition(const sf::Vector2f playerPos) { m_PlaySprite.setPosition(playerPos); };
	void SetPlaySpriteRect(int column, int row) { m_PlaySprite.setTextureRect(sf::IntRect(column * 46, row * 50, 46, 50)); };
	void FlipPlaySprite() { m_PlaySprite.scale(-1, 1); }

protected:
	int m_DamageReduction;

};