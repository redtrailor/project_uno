#include "stdafx.h"
#include "Collider.h"
#include <cmath>


Collider::Collider()
{
}

Collider::~Collider()
{
}

bool Collider::CheckCollision(sf::Rect<float>& boundingBox, sf::RectangleShape* shapeToCheck)
{

	//get half the Width and height of the Minkowski Difference
	double MDWidth = 0.5f * (boundingBox.width + shapeToCheck->getSize().x);
	double MDHeight = 0.5f * (boundingBox.height + shapeToCheck->getSize().y);

	//Get the center of the two objects being checked for collision
	sf::Vector2f BBCenter = sf::Vector2f(boundingBox.left + (boundingBox.width / 2), boundingBox.top + (boundingBox.height / 2));
	sf::Vector2f STCCenter = sf::Vector2f(shapeToCheck->getPosition().x + (shapeToCheck->getSize().x / 2), shapeToCheck->getPosition().y + (shapeToCheck->getSize().y / 2));

	//	 Get the difference between the centers of the two objects
	double differenceX = BBCenter.x - STCCenter.x;
	double differenceY = BBCenter.y - STCCenter.y;

	//calculator for detecting a collision up to 2 pixels below feet so that when pushed out of a bottom collision i still have
	// a bool to return if player is standing on the ground
	if (boundingBox.top + boundingBox.height +2 > shapeToCheck->getPosition().y &&
		differenceY < 0 &&
		abs(differenceX) <= MDWidth)
	{
		m_IsStanding = true;
	}
	
	//	If the distance from the origins is less than the dimensions of the MD 
	//	then a collision has occured
	if (abs(differenceX) <= MDWidth && abs(differenceY) <= MDHeight)
	{
		//  Calculation of points to determine the direction of the colliosion
		//	Borrowed code, now 100% sure about why
		//  Had to switch left/right from source, probably cause SFML coords
		double wy = MDWidth * differenceY;
		double hx = MDHeight * differenceX;

		if (wy > hx)
		{
			if (wy > -hx)
			{
				m_ContactTop = true;
				return true;
			}
			else
			{
				//printf("Contact Right\n");
				m_ContactRight = true;
				return true;
			}
		}
		else
		{
			if (wy > -hx)
			{
				//printf("Contact Left\n");
				m_ContactLeft = true;
				return true;
			}
			else
			{
				//printf("Contact Bottom\n");
				m_ContactBottom = true;
				return true;
			}
		}
		return false;
	}
}


void Collider::SetAllContactsFalse()
{
	m_ContactBottom = false;
	m_ContactTop = false;
	m_ContactLeft = false;
	m_ContactRight = false;

	m_IsStanding = false;
}
