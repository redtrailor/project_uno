#pragma once
#include "BaseEquipment.h"

class Weapon : public BaseEquipment
{
public:
	Weapon();
	virtual ~Weapon();
	void Init();
	virtual void Update(const sf::Vector2f playerPos, int column, int row, bool flipSprite, bool isAttacking);

	void SetPlaySpritePosition(const sf::Vector2f playerPos) { m_PlaySprite.setPosition(playerPos.x ,playerPos.y); };
	void SetPlaySpriteRect(int column, int row) { m_PlaySprite.setTextureRect(sf::IntRect((column - 2) * 60, row * 30, 60, 30)); };
	void FlipPlaySprite() { m_PlaySprite.scale(-1, 1); }

	int GetDamage() { return m_Damage; }
	void SetDamage(int val) { m_Damage = val; }

protected:
	int m_Damage;
};