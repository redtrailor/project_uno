#pragma once
#include "GameState.h"

class GameManager;

class GameStateStart : public GameState
{
public:
	GameStateStart(GameManager* game);
	~GameStateStart();

	virtual void HandleInput(sf::RenderWindow& window);
	virtual void Update(sf::Time& deltaTime, sf::RenderWindow& window);
	virtual void Draw(sf::Time& deltaTime, sf::RenderWindow& window);

	void BeginGame();

private:
	
};


