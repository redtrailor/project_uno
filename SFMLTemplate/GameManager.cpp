#include "stdafx.h"
#include "GameManager.h"



GameManager::GameManager()
{
	Init();
}

GameManager::~GameManager()
{
}

void GameManager::Play()
{
	while (window->isOpen())
	{
		m_DeltaTime = clock.restart();

		if (PeekState() == nullptr) continue;
		PeekState()->HandleInput(*window);
		PeekState()->Update(m_DeltaTime, *window);
		window->clear();
		PeekState()->Draw(m_DeltaTime, *window);
		window->display();

		//printf("position of window is : (%d,%d)\n", GetMousePosition(*window).x, GetMousePosition(*window).y);
	}
}

void GameManager::Init()
{
	window.reset(new sf::RenderWindow(sf::VideoMode(1280, 768), "Project UNO"));
	window->setFramerateLimit(120);

	m_GUI.setWindow(*window);

	if (!m_BackgroundTex.loadFromFile("Levels/background.png"))
		std::cout << "Failed to load background texture from file";

	m_Background.setTexture(m_BackgroundTex);
	m_Background.setScale(sf::Vector2f(2.0, 2.0));

	if (!m_Map.LoadFromFile("Levels/tileset.png", sf::Vector2u(64, 64), "Levels/level1.txt", collisionArray))
		std::cout << "Failed to load level" << std::endl;

	m_View.setCenter(m_Player.GetCurrentPosition().x, m_Player.GetCurrentPosition().y);
	m_View.setSize(sf::Vector2f(800, 500));
	m_View.zoom(1.5f);
};

void GameManager::PushState(GameState* state)
{
	m_States.push(state);
	return;
}

void GameManager::PopState()
{
	delete m_States.top();
	m_States.pop();
	return;
}

void GameManager::ChangeState(GameState* state)
{
	if (!m_States.empty())
	{
		PopState();
	}
	PushState(state);
	return;
}

GameState* GameManager::PeekState()
{
	if (m_States.empty()) return nullptr;
	return m_States.top();
}

sf::Vector2i GameManager::GetMousePosition(sf::RenderWindow& window)
{
	sf::Vector2i _windowBorderOffset = sf::Vector2i(8, 32);
	sf::Vector2i _globalMousePos = sf::Mouse::getPosition();
	_globalMousePos -= window.getPosition();
	_globalMousePos -= _windowBorderOffset;
	return _globalMousePos;
}
