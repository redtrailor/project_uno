#include "stdafx.h"
#include "GameStatePlaying.h"
#include "GameStatePause.h"


GameStatePlaying::GameStatePlaying(GameManager* game)
{
	m_Game = game;
}


GameStatePlaying::~GameStatePlaying()
{
}

void GameStatePlaying::HandleInput(sf::RenderWindow& window)
{
	sf::Event event;

	while (window.pollEvent(event))
	{
		switch (event.type)
		{

		case sf::Event::Closed:
		{
			window.close();
			break;
		}

		case sf::Event::KeyPressed:
		{
			if (event.key.code == sf::Keyboard::Escape)
				m_Game->PushState(new GameStatePause(m_Game));
			break;
		}

		}
	}
}

void GameStatePlaying::Update(sf::Time& deltaTime, sf::RenderWindow& window)
{
	//m_Game->Update(deltaTime);

	m_Game->m_Player.InputHandler(deltaTime);

	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();
	}

	sf::Vector2f currentPosition;
	currentPosition = m_Game->m_Player.playerSprite.getPosition();

	m_Game->m_View.setCenter(currentPosition.x, currentPosition.y);

	m_Game->m_Player.m_Collider.SetAllContactsFalse();
	m_Game->m_Player.SetStandingOnGround(false);

	//Loop through collision array
	for (int i = 0; i < 100; i++)
	{
		if (m_Game->collisionArray[i] != NULL)
		{
			(m_Game->m_Player.m_Collider.CheckCollision(m_Game->m_Player.GetBoundingBox(), m_Game->collisionArray[i]));
		}
	}
	if (m_Game->m_Player.m_Collider.GetContactBottom())
	{
		m_Game->m_Player.playerSprite.move(0, -1.f);
	}

	m_Game->m_Player.Update(event, deltaTime);
	m_Game->m_Player.SetPreviousPosition();
}

void GameStatePlaying::Draw(sf::Time& deltaTime, sf::RenderWindow& window)
{
	window.setView(m_Game->m_View);
	window.clear();
	window.draw(m_Game->m_Background);
	window.draw(m_Game->m_Map);
	window.draw(m_Game->m_Player.playerSprite);

	//render player equipment
	m_Game->m_Player.Render(window);
}