#pragma once
#include <memory>
#include "BaseEquipment.h"
#include "Brown_Robe.h"
#include "Leather_Chest.h"
#include "Steel_Sword.h"
#include "Leather_Helm.h"

class Player;

class EquippedInventory
{
public:
	EquippedInventory();
	~EquippedInventory();
	void Init();

	void Render(sf::RenderWindow& window);
	void Update(Player& player/*const sf::Vector2f playerPos, int column, int row, bool isAttacking*/);
	
	bool GetFlip() { return FlipSprite; }
	void SetFlip(bool newFlip) { FlipSprite = newFlip; }

private:
	
	typedef std::shared_ptr<BaseEquipment> equipPtr;

	equipPtr m_HeadSlot;
	equipPtr m_RobeSlot;
	equipPtr m_ChestSlot;
	equipPtr m_OffhandSlot;
	equipPtr m_WeaponSlot;
	equipPtr m_BootSlot;
	equipPtr m_RingOne;
	equipPtr m_RingTwo;
	equipPtr m_Amulet;

	bool FlipSprite;
};  

