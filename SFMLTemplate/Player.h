#pragma once
#include <SFML\Graphics.hpp>
#include <vector>
#include "PlayerState.h"
#include "StandingState.h"
#include "AirborneState.h"
#include "Collider.h"
#include "BaseEquipment.h"
#include "Armor.h"
#include "Brown_Robe.h"
#include "EquippedInventory.h"


class Player
{
public:
	Player();
	~Player();
	sf::Sprite playerSprite;
	Collider m_Collider;

	void InputHandler(sf::Time& deltaTime);
	void Update(sf::Event& event, sf::Time& deltaTime);
	void Render(sf::RenderWindow& window);

	int GetColumn() { return spriteColumn; }
	void SetColumn(int newColumn) { spriteColumn = newColumn; }

	int GetRow() { return spriteRow; }
	void SetRow(int newRow) { spriteRow = newRow; }

	bool GetFacingRight() { return m_isFacingRight; }
	void SetFacingRight(bool newDirection) { m_isFacingRight = newDirection; }

	bool GetStandingOnGround() { return m_isStandingOnGround; }
	void SetStandingOnGround(bool newStanding) { m_isStandingOnGround = newStanding; }

	bool GetAttacking() { return m_isAttacking; }
	void SetAttacking(bool newAttacking) { m_isAttacking = newAttacking; }

	float GetSpriteWidth() { return m_SpriteWidth; }
	float GetSpriteHeight() { return m_SpriteHeight; }

	float GetHorizontalMovementSpeed(){ return m_HorizontalMovementSpeed; }
	void SetHorizontalMovementSpeed(float NewHorizontalMovementSpeed) { m_HorizontalMovementSpeed = NewHorizontalMovementSpeed; }

	float GetVerticalMovementSpeed() { return m_VerticalMovementSpeed; }
	void SetVerticalMovementSpeed(float NewVerticalMovementSpeed) { m_VerticalMovementSpeed = NewVerticalMovementSpeed; }

	sf::Vector2f GetCurrentPosition() { return playerSprite.getPosition(); }
	void SetCurrentPosition(sf::Vector2f newPosition) { playerSprite.setPosition(newPosition); }

	sf::Vector2f GetPreviousPosition() { return m_PreviousPosition; }
	void SetPreviousPosition() { m_PreviousPosition = playerSprite.getPosition(); }

	sf::Rect<float> GetBoundingBox();
	float m_Gravity;

	//Stat getters
	int GetHealth() { return m_Health; }
	int GetAttack() { return m_Attack; }
	int GetDefense() { return m_Defense; }
	int GetMagic() { return m_Magic; }
	int GetStamina() { return m_Stamina; }
	//Stat Setters
	void SetHealth(int val) { m_Health = val; }
	void SetAttack(int val) { m_Attack = val; }
	void SetDefense(int val) { m_Defense = val; }
	void SetMagic(int val) { m_Magic = val; }
	void SetStamina(int val) { m_Stamina = val; }
	
	//EquippedInventory m_Equipped;
	EquippedInventory m_Equipped;

private:
	void Init();
	sf::Texture playerTexture;
	int spriteRow;
	int spriteColumn;
	float tempDeltaTime = 0;
	float m_SpriteWidth = 46;
	float m_SpriteHeight = 50;
	float m_HorizontalMovementSpeed = 0;
	float m_VerticalMovementSpeed = 0;

	sf::Vector2f m_PreviousPosition;
	sf::Vector2f m_CurrentPosition;

	//Stat definitions
	int m_Health;
	int m_Attack;
	int m_Defense;
	int m_Magic;
	int m_Stamina;

	PlayerState* state_;
	
	//Forward stuff for state push/pop back
	//nothing beyond this is implimented
	typedef std::vector<PlayerState*> m_StateVector;
	m_StateVector m_PreviousStates;

	bool m_isFacingRight;
	bool m_isStandingOnGround;
	bool m_isAttacking;


};