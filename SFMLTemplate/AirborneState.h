#pragma once
#include "PlayerState.h"
class AirborneState : public PlayerState
{
public:
	AirborneState();
	virtual ~AirborneState();
	virtual PlayerState* InputHandler(Player& player, PlayerState* state, sf::Time& deltaTime);
	virtual void Update(Player& player, sf::Time& deltaTime);
	virtual void Enter(Player& player, sf::Time& deltaTime);
	virtual void Exit(Player& player, sf::Time& deltaTime);

	virtual void ChangeState();

protected:
	
};