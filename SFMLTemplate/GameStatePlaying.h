#pragma once
#include "GameState.h"

class GameManager;

class GameStatePlaying : public GameState
{
public:
	GameStatePlaying(GameManager* game);
	~GameStatePlaying();

	virtual void HandleInput(sf::RenderWindow& window);
	virtual void Update(sf::Time& deltaTime, sf::RenderWindow& window);
	virtual void Draw(sf::Time& deltaTime, sf::RenderWindow& window);

};

