#pragma once
#include "stdafx.h"
#include "StandingState.h"
#include "RunningState.h"
#include "CrouchState.h"
#include "AirborneState.h"
#include "Player.h"


StandingState::StandingState()
{
}

StandingState::~StandingState()
{
}

PlayerState*  StandingState::InputHandler(Player& player, PlayerState* state, sf::Time& deltaTime)
{
	
	if (!player.GetAttacking())
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			return new CrouchState;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			return new StandingState;
		}
		else if ((sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::A)))
		{

			return new RunningState;
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			player.SetColumn(2);
			player.SetAttacking(true);
			//return new StandingState;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			player.SetVerticalMovementSpeed(-400);
			return new AirborneState;
		}
		//Jump(player);
	}

	return PlayerState::InputHandler(player, state,deltaTime);
	return nullptr;
}

void StandingState::Update(Player & player, sf::Time & deltaTime)
{
	Attack(player, deltaTime, 0, 0);
}

void StandingState::Enter(Player & player, sf::Time& deltaTime)
{
	
	isActive = true;
	isMovingLeft = false;
	isMovingRight = false;
	m_StateHorizontalSpeed = 0;
	player.SetRow(0);
	player.SetHorizontalMovementSpeed(0);
	player.SetVerticalMovementSpeed(0);
	player.SetStandingOnGround(true);

	if (!player.GetAttacking())
	{
		player.SetColumn(0);
	}
}

void StandingState::Exit(Player & player, sf::Time & deltaTime)
{

}

void StandingState::ChangeState()
{
}
