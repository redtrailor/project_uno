#pragma once
#include <SFML\Graphics.hpp>
class Player;
class PlayerState
{
public:
	PlayerState();
	virtual ~PlayerState();
	virtual PlayerState* InputHandler(Player& player, PlayerState* state, sf::Time& deltaTime);
	virtual void Update(Player& player, sf::Time& deltaTime);
	virtual void Enter(Player& player, sf::Time& deltaTime);
	virtual void Exit(Player& player, sf::Time& deltaTime);


	virtual void ChangeState();
	bool isActive = false;

	void HorizontalDirectionHandler(Player& player, sf::Time& deltaTime);
	void HorizontalMoveHandler(Player& player, sf::Time& deltaTime);

	void Attack(Player& player, sf::Time& deltaTime, int endColumn, int endRow);

	PlayerState* Jump(Player& player);

protected:
	float tempDeltaTime;
	int m_StateHorizontalSpeed;

	bool isMovingLeft = false;
	bool isMovingRight = false;
};