#include "stdafx.h"
#include "GameStateStart.h"
#include "GameStatePlaying.h"


GameStateStart::GameStateStart(GameManager* game)
{
	m_Game = game;
}


GameStateStart::~GameStateStart()
{
}

void GameStateStart::HandleInput(sf::RenderWindow& window)
{
	sf::Event event;

	while (window.pollEvent(event))
	{
		switch (event.type)
		{

		case sf::Event::Closed:
		{
			window.close();
			break;
		}

		case sf::Event::KeyPressed:
		{
			if (event.key.code == sf::Keyboard::Escape) window.close();
			else if (event.key.code == sf::Keyboard::Space) BeginGame();
			break;
		}

		}
	}
}

void GameStateStart::Update(sf::Time& deltaTime, sf::RenderWindow& window)
{

}

void GameStateStart::Draw(sf::Time& deltaTime, sf::RenderWindow& window)
{
	window.clear();
	window.draw(m_Game->m_Background);
}

void GameStateStart::BeginGame()
{
	m_Game->PushState(new GameStatePlaying(m_Game));
}
